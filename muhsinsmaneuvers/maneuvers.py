from pid2 import PID
import numpy as np
from math import *
import krpc
import time
import sys
sys.path.append('..')

# https://wiki.kerbalspaceprogram.com/wiki/Synchronous_orbit
geostationary_orbits = {
    'Kerbol':	1508045.29 * 1000,
    'Moho':	    18173.17 * 1000,
    'Eve':	    10328.47 * 1000,
    'Gilly':	42.14 * 1000,
    'Kerbin':	2863.33 * 1000,
    'Mun':	    2970.56 * 1000,
    'Minmus':	357.94 * 1000,
    'Duna':	    2880.00 * 1000,
    'Ike':	    1133.90 * 1000,
    'Dres':	    732.24 * 1000,
    'Jool':	    15010.46 * 1000,
    'Laythe':	4686.32 * 1000,
    'Vall':	    3593.20 * 1000,
    'Tylo':	    14157.88 * 1000,
    'Bop':	    2588.17 * 1000,
    'Pol':	    2415.08 * 1000,
    'Eeloo':	683.69 * 1000
}

class bruh():
    def __init__(self, conn, vessel):
        self.conn = conn
        self.vessel = vessel
        pass

    def stage_if_needed(self):
        vessel = self.vessel
        Isp = vessel.specific_impulse
        if Isp == 0.0:
            print('staging automatically...')
            vessel.control.activate_next_stage()
            time.sleep(.1)


    def change_ap_at_pa(self, params):
        new_ap = int(params['new_ap'])
        conn = self.conn
        vessel = self.vessel
        vessel.control.sas = True
        vessel.auto_pilot.disengage()
        self.stage_if_needed()
        G = conn.space_center.g
        M = vessel.orbit.body.mass
        R = vessel.orbit.body.equatorial_radius
        μ = M*G

        ap = vessel.orbit.apoapsis_altitude
        pa = vessel.orbit.periapsis_altitude

        r1 = pa + R
        r2 = ap + R
        rs = vessel.orbit.semi_major_axis
        V_pa = sqrt(μ*(2/r1 - 1/rs))
        V_ap = sqrt(μ*(2/r2 - 1/rs))

        r3 = new_ap + R
        rs_new = (r1 + r3) / 2
        V_pa_new = sqrt(μ*(2/r1 - 1/rs_new))

        dV = V_pa_new - V_pa

        ut = conn.space_center.ut
        t_pa = vessel.orbit.time_to_periapsis
        node = vessel.control.add_node(ut + t_pa, prograde=dV)
        burn_time = calculate_burn_time(vessel, dV)
        if(burn_time == 0.):
            return

        burn_ut = ut + t_pa - burn_time/2.
        lead_time = 60
        conn.space_center.warp_to(burn_ut - lead_time)

        auto_pilot = vessel.auto_pilot
        auto_pilot.sas = True

        auto_pilot.sas_mode = auto_pilot.sas_mode.maneuver
        

        t_now = conn.add_stream(getattr, conn.space_center, 'ut')
        while t_now() < burn_ut:
            time.sleep(.1)
        
        print('executing burn')
        throttlePID = PID(LastInput=0.0,
                        LastSampleTime = time.time(),
                        ErrorSum = 0.0,
                        Kp = 1,
                        Ki = 0.001,
                        Kd = 0.01,
                        Setpoint = 0.,
                        MinOutput = 0.00,
                        MaxOutput = 1.0,
                        Maxdt = 1)
        
        remaining_burn = conn.add_stream(node.remaining_burn_vector, node.reference_frame)
        rem_burn_0 = np.linalg.norm(remaining_burn())
        while(True):
            rem_burn = np.linalg.norm(remaining_burn())
            if(rem_burn - 0.5 > 0):
                throttle = throttlePID.update(time.time(), -(rem_burn/rem_burn_0)*3)
                vessel.control.throttle = throttle
            else:
                break
            if(vessel.available_thrust <= 0):
                vessel.control.throttle = 0.
                time.sleep(1)
                vessel.control.activate_next_stage()
                time.sleep(2)
        vessel.control.throttle = 0.
        node.remove()

    def change_pa_at_ap(self, params):
        new_ap = int(params['new_pa'])
        conn = self.conn
        vessel = self.vessel
        vessel.control.sas = True
        vessel.auto_pilot.disengage()
        self.stage_if_needed()
        G = conn.space_center.g
        M = vessel.orbit.body.mass
        R = vessel.orbit.body.equatorial_radius
        μ = M*G

        ap = vessel.orbit.apoapsis_altitude
        pa = vessel.orbit.periapsis_altitude

        r1 = pa + R
        r2 = ap + R
        rs = vessel.orbit.semi_major_axis
        V_pa = sqrt(μ*(2/r1 - 1/rs))
        V_ap = sqrt(μ*(2/r2 - 1/rs))

        r3 = new_ap + R
        rs_new = (r2 + r3) / 2
        V_ap_new = sqrt(μ*(2/r2 - 1/rs_new))

        dV = V_ap_new - V_ap
        
        print('V_pa: ', V_pa)
        print('V_pa_new: ', V_ap_new)
        print('dV: ', dV)

        
        ut = conn.space_center.ut
        t_ap = vessel.orbit.time_to_apoapsis
        node = vessel.control.add_node(ut + t_ap, prograde=dV)
        burn_time = calculate_burn_time(vessel, dV)
        if(burn_time == 0.):
            return

        burn_ut = ut + t_ap - burn_time/2.
        lead_time = 60
        conn.space_center.warp_to(burn_ut - lead_time)

        auto_pilot = vessel.auto_pilot
        auto_pilot.sas = True

        auto_pilot.sas_mode = auto_pilot.sas_mode.maneuver
        

        t_now = conn.add_stream(getattr, conn.space_center, 'ut')
        while t_now() < burn_ut:
            time.sleep(.1)
        
        print('executing burn')
        throttlePID = PID(LastInput=0.0,
                        LastSampleTime = time.time(),
                        ErrorSum = 0.0,
                        Kp = 1,
                        Ki = 0.001,
                        Kd = 0.01,
                        Setpoint = 0.,
                        MinOutput = 0.00,
                        MaxOutput = 1.0,
                        Maxdt = 1)
        
        remaining_burn = conn.add_stream(node.remaining_burn_vector, node.reference_frame)
        rem_burn_0 = np.linalg.norm(remaining_burn())
        while(True):
            rem_burn = np.linalg.norm(remaining_burn())
            if(rem_burn - 0.5 > 0):
                throttle = throttlePID.update(time.time(), -(rem_burn/rem_burn_0)*3)
                vessel.control.throttle = throttle
            else:
                break
            if(vessel.available_thrust <= 0):
                vessel.control.throttle = 0.
                time.sleep(1)
                vessel.control.activate_next_stage()
                time.sleep(2)
        vessel.control.throttle = 0.
        node.remove()
        
    def circularize(self, params):
        at: str = params['at']
        """circularize orbit
        
        Arguments:
            at {str} -- circularize orbit at either: 'periapsis' or 'apoapsis'
        """
        if at == 'periapsis':
            pa = vessel.orbit.periapsis_altitude
            self.change_ap_at_pa({'new_ap': pa})
        elif at == 'apoapsis':
            ap = vessel.orbit.apoapsis_altitude
            self.change_pa_at_ap({'new_pa': ap})
    
    def transfer_to_geostationary(self):
        vessel = self.vessel
        body_name = vessel.orbit.body.name
        if body_name not in geostationary_orbits:
            print('No geostationary orbit defined for ', body_name, '. Terminating...')
            return
        geo_r = geostationary_orbits[body_name]
        
        self.change_ap_at_pa({'new_ap':geo_r})
        
        pa = vessel.orbit.periapsis_altitude
        ap = vessel.orbit.apoapsis_altitude
        at = 'periapsis' if abs(pa-geo_r) < abs(ap-geo_r) else 'apoapsis'
        self.circularize({'at':at})

    def escape(self):
        self.stage_if_needed()
        conn = self.conn
        vessel = self.vessel
        G = conn.space_center.g
        M = vessel.orbit.body.mass
        R = vessel.orbit.body.equatorial_radius
        μ = M*G

        ap = vessel.orbit.apoapsis_altitude
        pa = vessel.orbit.periapsis_altitude

        ut = conn.space_center.ut
        r = vessel.orbit.radius_at(ut + 10)
        V = vessel.orbit.orbital_speed_at(ut + 10)
        V_esc = np.sqrt(2*μ/r)

        dV = V_esc - V
        node = vessel.control.add_node(ut + 10, prograde=dV)
        burn_time = calculate_burn_time(vessel, dV)
        if(burn_time == 0.):
            return

        burn_ut = ut - burn_time/2.
        
        auto_pilot = vessel.auto_pilot
        auto_pilot.sas = True

        auto_pilot.sas_mode = auto_pilot.sas_mode.maneuver


        t_now = conn.add_stream(getattr, conn.space_center, 'ut')
        while t_now() < burn_ut:
            time.sleep(.1)
        
        print('executing burn')
        throttlePID = PID(LastInput=0.0,
                        LastSampleTime = time.time(),
                        ErrorSum = 0.0,
                        Kp = 1,
                        Ki = 0.001,
                        Kd = 0.01,
                        Setpoint = 0.,
                        MinOutput = 0.00,
                        MaxOutput = 1.0,
                        Maxdt = 1)
        remaining_burn = conn.add_stream(node.remaining_burn_vector, node.reference_frame)
        rem_burn_0 = np.linalg.norm(remaining_burn())
        while(True):
            rem_burn = np.linalg.norm(remaining_burn())
            if(rem_burn - 0.5 > 0):
                throttle = throttlePID.update(time.time(), -(rem_burn/rem_burn_0)*3)
                vessel.control.throttle = throttle
            else:
                break
            if(vessel.available_thrust <= 0):
                vessel.control.throttle = 0.
                time.sleep(1)
                vessel.control.activate_next_stage()
                time.sleep(2)
        vessel.control.throttle = 0.
        node.remove()


    def wait(self, params):
        t = int(params['seconds'])
        time.sleep(t)
    
    def launch_into_orbit(self):
        conn = self.conn
        vessel = self.vessel
        turn_start_alt = 250
        turn_end_alt = 45000
        target_alt = 150000
        
        conn = krpc.connect(name='Launch into orbit')
        vessel = conn.space_center.active_vessel

        canvas = conn.ui.stock_canvas
        screen_size = canvas.rect_transform.size

        panel = canvas.add_panel()
        rect = panel.rect_transform
        rect.size = (200, 100)
        rect.position = (110-(screen_size[0]/2), 0)

        text = panel.add_text("Roll: ")
        text.rect_transform.position = (0, -20)
        text.color = (1, 1, 1)
        text.size = 18

        roll = conn.add_stream(getattr, vessel.flight(), 'roll')
        prev_roll = roll()
        text.content = "Roll: {0}".format(roll())


        ut = conn.add_stream(getattr, conn.space_center, 'ut') # time (now) in game
        alt = conn.add_stream(getattr, vessel.flight(), 'mean_altitude')
        ap = conn.add_stream(getattr, vessel.orbit, 'apoapsis_altitude')
        # stage_1_resources = vessel.resources_in_stage
        

        vessel.control.sas = False
        vessel.control.rcs = False
        vessel.control.throttle = 1.0

        for i in range(1, 3, -1):
            print('{0}...' % i)
            time.sleep(1)
        print('Launch!')

        # fire engines
        vessel.control.activate_next_stage()
        # disengage clams
        vessel.control.activate_next_stage()
        vessel.auto_pilot.engage()
        vessel.auto_pilot.target_pitch_and_heading(90,90)

        stage_2_resources = vessel.resources_in_decouple_stage(stage=4, cumulative=False)
        srb_fuel = conn.add_stream(stage_2_resources.amount, 'LiquidFuel')
        srb_first = srb_fuel()
        print("fuel:", srb_fuel())


        srbs_seperated = False
        turn_angle = 0
        while True:
            cur_roll = roll()

            if abs(cur_roll - prev_roll) > 0.01:
                prev_roll = cur_roll
                text.content = "Roll: {0}".format(cur_roll)
            if(alt() > turn_start_alt and alt() < turn_end_alt):
                frac = (alt() - turn_start_alt) / (turn_end_alt - turn_start_alt)
                new_turn_angle = frac * 90
                if abs(new_turn_angle - turn_angle) > 0.5:
                    turn_angle = new_turn_angle
                    vessel.auto_pilot.target_pitch_and_heading(90-turn_angle, 90)
                    vessel.auto_pilot.target_roll = 0
            # seperate SRBs when they run out of fuel
            if not srbs_seperated:
                if(srb_fuel() / srb_first < 0.1):
                    vessel.control.throttle = 0.0
                    time.sleep(1.0)
                    # stabilize
                    vessel_direction = vessel.direction(vessel.surface_reference_frame)
                    vessel.auto_pilot.target_direction = vessel_direction
                    vessel.auto_pilot.wait() # wait until stable
                    
                    vessel.control.activate_next_stage() # ditch srbs
                    srbs_seperated = True
                    print('SRBs seperated')
                    vessel.control.activate_next_stage() # enable first stage engine
                    time.sleep(0.3)
                    vessel.control.throttle = 1.0
            # decrease throttle when approaching target ap
            if ap() > target_alt*0.9:
                print('Approaching target apoapsis')
                break
        # vessel.control.throttle = .25
        while ap() < target_alt:
            time.sleep(.1)
        print('Target apoapsis reached')
        vessel.control.throttle = 0.0

        print('Coasting out of athmosphere')
        while alt() < 70500: # atmosphere is 70000
            pass
        vessel.auto_pilot.disengage()


def calculate_burn_time(vessel, dV):
    G = vessel.orbit.body.gravitational_parameter
    F = vessel.available_thrust
    Isp = vessel.specific_impulse * G
    if(Isp == 0):
        print('terminating: no active engine!')
        return 0
    m0 = vessel.mass
    m1 = m0 / exp(dV/Isp)
    flow_rate = F/Isp
    burn_time = (m0-m1) / flow_rate
    return burn_time



conn = krpc.connect()
sc = conn.space_center
vessel = sc.active_vessel
# change_ap_at_pa(conn,vessel,200000)
