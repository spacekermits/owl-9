the plan object used in demo is in src/main.py, plan3.
Here is a copy of the plan:
```python
plan3 = [{'name': 'launch_into_orbit'},
         {'name': 'circularize', 'at': 'apoapsis'},
         {'name': 'wait', 'seconds': 60},
         {'name': 'transfer_to_geostationary'},
         {'name': 'wait', 'seconds': 60},
         {'name': 'escape'}]
```