import krpc
import time
import math
import json
from typing import Callable
import read
import urllib
import numpy as np
#from pid2 import PID
from pid import PID


class Maneuver(object):
    name: str
    procedure: Callable
    params: dict

    def __init__(self, name, procedure, params=None):
        self.name = name
        self.procedure = procedure
        self.params = params

    def get_procedure(self):
        return self.procedure

    def get_procedure_name(self):
        return self.name

    def execute(self):
        if self.params != None:
            self.procedure(self.params)
        else:
            self.procedure()


class FlightPlan():
    maneuvers: [Maneuver] = []

    def __init__(self):
        pass

    def add_stage(self, maneuver):
        self.maneuvers.append(maneuver)

    def activate_next(self):
        for man in self.maneuvers:
            print("Executing", man.get_procedure_name())
            man.execute()

    def execute_all(self):
        self.activate_next()


class JSON_object(object):

    def __init__(self, class_name=None):
        self.Action = class_name
        pass

    def create_maneuvers(self, fn_names, parameters):
        f = FlightPlan()
        if self.Action is None:
            self.Action = Action()
        for i in fn_names:
            if i in parameters:
                string = 'self.Action.' + i
                maneuver = Maneuver(name=i, procedure=eval(string), params=parameters[i])
            else:
                string = "self.Action." + i
                maneuver = Maneuver(name=i, procedure=eval(string))
            f.add_stage(maneuver)
        f.execute_all()

    def read_json(self, filename):
        fn_names = []
        params = {}
        with open(filename) as json_file:
            data = json.load(json_file)
            for p in data['maneuvers']:
                name = p['name']
                fn_names.append(name)
                if len(p.keys()) > 1:
                    del p['name']
                    params[name] = p
        return fn_names, params


class Action():

    def __init__(self):
        self.PI = 3.14159
        self.conn = krpc.connect(rpc_port=50000, stream_port=50001)
        self.vessel = read.Game_data(self.conn).get_vessel()
        self.game_data = read.Game_data(self.conn)
        self.vessel_control = read.vessel_data(self.vessel)
        self.error = 0

    def burn_times(self):
        sc = self.conn.space_center
        rf = self.vessel.orbit.body.reference_frame
        alt = 5000
        # calculate sin of angle from horizontal -
        v1 = self.vessel.velocity(rf)
        v2 = (0, 0, 1)
        v1 = v1 / np.linalg.norm(v1)
        v2 = v2 / np.linalg.norm(v2)
        angle_between = np.arccos(np.clip(np.dot(v1, v2), -1.0, 1.0))
        angle_from_horizontal = angle_between
        sine = math.sin(angle_from_horizontal)

        # estimate deceleration time
        g = self.vessel.orbit.body.surface_gravity
        T = (self.vessel.max_thrust / self.vessel.mass) * .95  # calculating with 5% safety margin!
        effective_decel = .5 * (-2 * g * sine + math.sqrt((2 * g * sine)
                                                          * (2 * g * sine) + 4 * (T*T - g*g)))
        decel_time = self.vessel.flight(rf).speed / effective_decel

        # estimate time until burn
        radius = self.vessel.orbit.body.equatorial_radius + alt
        TA = self.vessel.orbit.true_anomaly_at_radius(radius)
        TA = -1 * TA  # look on the negative (descending) side of the orbit
        impact_time = self.vessel.orbit.ut_at_true_anomaly(TA)
        burn_time = impact_time - decel_time/2
        ground_track = ((burn_time - sc.ut) * self.vessel.flight(rf).speed) + \
            (.5 * self.vessel.flight(rf).speed * decel_time)
        return burn_time - sc.ut

    def inclination(self, params):
        user_input = params['user_input']
        angle = int(params['angle'])
        if angle > 45:
            angle = angle - 45
        # go to a certain angle in the north/equator/south hemisphere
        if user_input == "north":
            if self.vessel.control.current_stage == 3:
                self.vessel.control.activate_next_stage()
            user_input = "Normal"
            conversion_to_angle = self.vessel.orbit.inclination * (180/self.PI)
            self.vessel_control.change_sas_mode(self.vessel, user_input)
            time.sleep(0.1)
            while angle > conversion_to_angle:
                self.vessel.control.throttle = 1
                conversion_to_angle = self.vessel.orbit.inclination * (180/self.PI)
            self.vessel.control.throttle = 0
        elif user_input == "equator":
            user_input = "None"
        elif user_input == "south":
            if self.vessel.control.current_stage == 3:
                self.vessel.control.activate_next_stage()
            user_input = "Anti-Normal"
            self.vessel_control.change_sas_mode(self.vessel, user_input)
            time.sleep(0.1)
            conversion_to_angle = self.vessel.orbit.inclination * (180/self.PI)
            while angle > conversion_to_angle:
                self.vessel.control.throttle = 1
                conversion_to_angle = self.vessel.orbit.inclination * (180/self.PI)
            self.vessel.control.throttle = 0
        else:
            print("wrong input")
            exit()

    def de_orbit(self):
        time_ = self.game_data.get_time()

        # enables sas
        self.vessel_control.enable_sas_mode(self.vessel)
        if self.vessel.control.current_stage == 3:
            self.vessel.control.activate_next_stage()
        self.vessel.control.speed_mode = self.conn.space_center.SpeedMode.orbit

        time_future = time_ + (5*60)
        maneuver_vector = 180
        node = self.vessel_control.maneuver_node(time_future, maneuver_vector)

        # point sas to the maneuver node
        self.vessel_control.change_sas_mode(self.vessel, "maneuver")

        # calculates burn time till it arrives to maneuver node
        burn_time = math.ceil(maneuver_vector/22.5)

        # time warp to maneuver node
        self.game_data.time_warp(time_ + node.time_to - burn_time - 3, 100)

        while True:
            time_ = self.game_data.get_time()

            # # matches the burn time with the maneuver node time
            if int(burn_time) in range(int(node.time_to-0.01), int(node.time_to+0.01)):
                self.vessel.control.throttle = 1
                time.sleep(burn_time+1)
                self.vessel.control.throttle = 0
                break

    def list_available_maneuvers(self):
        print("The available maneuvers are listed:")
        print("1. North Maneuver")
        print("2. Equator Maneuver")
        print("3. South Maneuver")
        print("Inclination can be any value between 0 and 90")

    def return_error(self):
        return self.error

    def landing(self):

        if self.vessel.orbit.periapsis_altitude < 10000.0:
            self.vessel_control.change_sas_mode(self.vessel, "retrograde")
            self.vessel.control.speed_mode = self.conn.space_center.SpeedMode.surface

            # fast forward until landing
            time_ = self.game_data.get_time()
            self.game_data.time_warp(time_ + (35 * 60), 100)

            telem = self.vessel.flight(self.vessel.orbit.body.reference_frame)
            time.sleep(0.5)
            p = PID(0.25, 0.25, 0.025)
            on = False
            altitude = telem.surface_altitude

            # initiate thrusters for landing
            while self.vessel.situation is not self.vessel.situation.landed:
                time.sleep(0.01)
                if self.burn_times() < -8.5:
                    on = True
                if on is True:
                    descent = telem.surface_altitude / -10.0
                    if descent < -15.0:
                        descent = -15.0
                    p.set_point(descent)
                    self.error = p.get_error()
                    self.vessel.control.throttle = p.compute(telem.vertical_speed)
                    print("BURN TIME IS", self.burn_times())

            self.vessel.control.throttle = 0
        else:
            print("Can't land because it's not de-orbitted", self.vessel.orbit.periapsis_altitude)
