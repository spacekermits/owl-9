### Using ckan on OSX

```bash
brew install mono
brew install ckan
# done!


ckan --help
# should see available commands
```


```bash
# add ksp folder
ckan ksp add owl <ksp folder location>      # e.g, mine: /Users/muhsinfatih/Library/Application Support/Steam/steamapps/common/Kerbal Space Program
ckan ksp list

ckan ksp default owl
ckan ksp list

ckan scan
ckan available      # lists available mods for installed ksp version
ckan install <mod name> # e.g: MechJeb2     # if MechJeb2 is already installed manually, ckan will not touch it. remove manually installed version first

ckan available | grep -i <modname>      # convenience, to find mods
ckan list       # list installed mods


```