# OWL-9

## demo:
https://youtu.be/rWamMTMuaQk

Folder structure:

```c
├── GameData            # common folder for sharing mod files and configurations.   
│                         Doesn't include the Squad folder because its too
│                         big! Beware!  
└── src
    └── test_files      # any random testing like tutorial files, API tests etc. 
                          put garbage files here as well.
```

Multiplayer server (because this way we don't have to sync save files every time + it's more fun!)
KSP DarkMultiPlayer mod running on Linux box on DigitalOcean with config:

```properties
name: OWL-9 server
ip: 134.209.127.54
port: 6702
```

TODO:
- planning

Useful resources:
- https://github.com/krpc/krpc-library
