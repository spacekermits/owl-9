import krpc


def resource_types(vessel):
    return vessel.resources.names

def get_fuel(vessel):
    return vessel.resources.amount('SolidFuel')