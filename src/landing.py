import krpc
import math
import time
import numpy as np

import sys
sys.path.append('..')

from pid2 import *


def main():
	conn = krpc.connect()
	sc = conn.space_center
	v = sc.active_vessel

	deorbit(v,.3)
	suicide_burn(conn, v)
	final_descent(v)



def deorbit(vessel, eccentricity):
	'''executes a deorbit burn until the given eccentricity
	is achieved.   If periapsis remains above the equatorial
	radius, it continues burning.
	'''
	print('de-orbiting vessel...')
	vessel.control.speed_mode = vessel.control.speed_mode.surface
	ap = vessel.auto_pilot
	ap.sas = True
	time.sleep(1)
	ap.sas_mode = ap.sas_mode.retrograde
	ap.wait()
	throttlePID = PID(LastInput=0.0,
										LastSampleTime = time.time(),
										ErrorSum = 0.0,
										Kp = 0.3 * 1/10000,
										Ki = 0.001 * 1/10000,
										Kd = 0.01 * 1/10000,
										Setpoint = 0.4,
										MinOutput = 0.00,
										MaxOutput = 1.0,
										Maxdt = 1)
	
	first_pa = vessel.orbit.periapsis_altitude
	while(vessel.orbit.periapsis_altitude > 10000):
		vessel.control.throttle = 1.
		time.sleep(.01)

	throttlePID.Setpoint = -0.
	while(True):
		pa_throttle = throttlePID.update(time.time(), -vessel.orbit.periapsis_altitude)
		if(vessel.orbit.periapsis_altitude <= 0.3):
			vessel.control.throttle = 0.
			break
		vessel.control.throttle = pa_throttle
		time.sleep(.01)
		

def suicide_burn(conn, vessel, autowarp = True):
	print('Calculating suicide burn')
	telem = vessel.flight(vessel.orbit.body.reference_frame)
	vessel.control.speed_mode = vessel.control.speed_mode.surface
	rf = vessel.orbit.body.reference_frame
	ap = vessel.auto_pilot
	ap.sas = True
	time.sleep(.1)
	ap.sas_mode = ap.sas_mode.retrograde

	computer = suicide_burn_calculator(conn, vessel, 5000)
	computer.update()
	touchdown = coords_down_bearing(telem.latitude, telem.longitude, (180 + telem.heading), computer.ground_track, vessel.orbit.body)

	computer.alt = check_terrain(telem.latitude, telem.longitude, touchdown[0], touchdown[1], vessel.orbit.body)

	#Initial burn - aiming for the 'safe alt' (highest point over course)
	# and a vertical velocity of 10 m/s
	burn_until_velocity(conn, vessel, telem, 10, computer, True)
	
	## update computer ti aim for 10m above current altitude.
	computer.alt = vessel.orbit.body.surface_height(telem.latitude, telem.longitude) + 10

	#Second half of burn - aiming for ground alt + 10m and a vertical
	#velocity of 5 m/s
	burn_until_velocity(conn, vessel, telem, 5, computer, True)
	

def burn_until_velocity(conn, vessel, telem, thresh, computer, autowarp):
	'''
	Helper Function for suicide_burn function - actually executes a burn
	at the calculated time and burns until it reaches the threshold
	vertical velocity.

	'''
	countdown = computer.update()
	print ("Burn in {} seconds".format(countdown))

	if autowarp and (countdown > thresh):
					conn.space_center.warp_to(conn.space_center.ut + countdown - 10)  # warp to 10 seconds before burn
					
	while countdown > 0.0:  #  Wait until suicide burn
					time.sleep(.1)
					countdown = computer.update()
					
	while telem.vertical_speed < (-1 * thresh):  #Loop until we're ready for final descent
					countdown = computer.update()
					vessel.control.throttle= .95  #95% throttle 
					if countdown < 0.0:  #use the emergencty 5% of throttle when needed
									vessel.control.throttle = 1.0 
	vessel.control.throttle = 0.0

def coords_down_bearing(lat, lon, bearing, distance, body):
	'''
	Takes a latitude, longitude and bearing in degrees, and a
	distance in meters over a given body.  Returns a tuple
	(latitude, longitude) of the point you've calculated.
	'''
	bearing = math.radians(bearing)
	R = body.equatorial_radius
	lat = math.radians(lat)
	lon = math.radians(lon)

	lat2 = math.asin( math.sin(lat)*math.cos(distance/R) +
								math.cos(lat)*math.sin(distance/R)*math.cos(bearing))

	lon2 = lon + math.atan2(math.sin(bearing)*math.sin(distance/R
									)*math.cos(lat),math.cos(distance/R)-math.sin(lat
									)*math.sin(lat2))

	lat2 = math.degrees(lat2)
	lon2 = math.degrees(lon2)
	return (lat2, lon2)

def check_terrain(lat1, lon1, lat2, lon2, body):
	'''
	Returns an estimate of the highest terrain altitude betwen
					two latitude / longitude points.
	'''
	lat = lat1
	lon = lon1
	highest_lat = lat
	highest_lon = lon
	highest_alt = body.surface_height(lat, lon)
	latstep = (lat2 - lat1) / 20
	lonstep = (lon2 - lon1) / 20

	for x in range (20):
					test_alt = body.surface_height(lat, lon)
					if  test_alt> highest_alt:
									highest_lat = lat
									highest_lon = lon
									highest_alt = test_alt
					lat = lat+latstep
					lon = lon+lonstep
	return highest_alt


class suicide_burn_calculator(object):
	def __init__(self, conn, v, alt):
		self.conn = conn
		self.v = v
		self.sc = conn.space_center
		self.burn_time = np.inf
		self.burn_duration = np.inf
		self.ground_track = np.inf
		self.effective_decel = np.inf
		self.radius = np.inf
		self.angle_from_horizontal = np.inf
		self.impact_time = np.inf
		self.alt = alt

		self.rf = self.sc.ReferenceFrame.create_hybrid(
			position=self.v.orbit.body.reference_frame,
			rotation=self.v.surface_reference_frame)
	
	def update(self):
		if self.v.orbit.periapsis_altitude > 0:
			self.burn_time = np.inf
			self.burn_duration = np.inf
			self.ground_track = np.inf
			self.effective_decel = np.inf
			self.angle_from_horizontal = np.inf
			self.impact_time = np.inf
			return self.burn_time
		rf = self.v.orbit.body.reference_frame
		v1 = self.v.velocity(rf)
		v2 = (0,0,1)
		self.angle_from_horizontal = angle_between(v1,v2)
		sin_theta = math.sin(self.angle_from_horizontal)
		cos_theta = math.cos(self.angle_from_horizontal)

		# estimate deceleration time
		g = self.v.orbit.body.surface_gravity
		a = (self.v.max_thrust / self.v.mass) * .95 # calculate with 5% safety margin
		
		t = self.v.flight(self.rf).speed * (cos_theta/2) / (2*a)
		x = (a * t**3)/(3*cos_theta)
		print('angle: ', self.angle_from_horizontal)
		h = x * (g - a*sin_theta)*cos_theta/(a*sin_theta**2)
		self.decel_time = t
		
		# estimate time until burn
		r = self.v.orbit.body.equatorial_radius + self.alt
		TA = self.v.orbit.true_anomaly_at_radius(r)
		TA = -1 * TA
		self.impact_time = self.v.orbit.ut_at_true_anomaly(TA)
		self.burn_time = self.impact_time - self.decel_time
		self.ground_track = ((self.burn_time- self.sc.ut) * self.v.flight(self.rf).speed) + (
												.5 * self.v.flight(self.rf).speed * self.decel_time)

def unit_vector(vector):
	""" Returns the unit vector of the vector provided.  """
	return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
	""" Returns the angle in radians between vectors 'v1' and 'v2'"""
	return np.arccos(np.clip(unit_vector(np.dot(v1, v2)), -1.0, 1.0))

if __name__ == "__main__":
	main()