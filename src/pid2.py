import krpc
import numpy as np
import time
from math import *
from dataclasses import dataclass

debug = True

class PID:
	@dataclass
	class initstruct:
		LastInput: float
		LastSampleTime: float
		ErrorSum: float
		Kp: float
		Ki: float
		Kd: float
		Setpoint: float
		MinOutput: float
		MaxOutput: float
		Maxdt: float

	def __init__(self, LastInput, LastSampleTime, ErrorSum, Kp, Ki, Kd, Setpoint, MinOutput, MaxOutput, Maxdt):
		self.LastInput = LastInput
		self.LastSampleTime = LastSampleTime
		self.ErrorSum = ErrorSum
		self.Kp = Kp
		self.Ki = Ki
		self.Kd = Kd
		self.Setpoint = Setpoint
		self.MinOutput = MinOutput
		self.MaxOutput = MaxOutput
		self.Maxdt = Maxdt
		self.PTerm = 0
		self.ITerm = 0
		self.DTerm = 0
		self.deadband = 0

	def update(self, t, x):
		e = self.Setpoint - x
		self.PTerm = self.Kp * e
		self.ITerm = 0
		self.DTerm = 0
		in_deadband = abs(e) < self.deadband
		dt = t - self.LastSampleTime

		if dt > self.Maxdt:
			self.reset(t)
			return self.update(t,x)

		if self.LastSampleTime < t and not in_deadband:
			if abs(e) > 10**-6:
				self.ITerm = (self.ErrorSum + e*dt)*self.Ki
			
			dx = (x - self.LastInput)/dt
			self.DTerm = -dx*self.Kd
		y = self.PTerm + self.ITerm + self.DTerm
		if debug:
			print('error: ', e)
			print('PTerm: ', self.PTerm)
			print('ITerm: ', self.ITerm)
			print('DTerm: ', self.DTerm)
		# prevent integral wind-up
		if y > self.MaxOutput:
			y = self.MaxOutput
			if self.Ki != 0 and self.LastSampleTime<t:
				self.ITerm = y - min(self.PTerm+self.DTerm, self.MaxOutput)
		elif y < self.MinOutput:
			y = self.MinOutput
			if self.Ki != 0 and self.LastInput<t:
				self.ITerm = y - max(self.PTerm+self.DTerm, self.MinOutput)
		
		if debug:
			print('output: ', y)
			
		self.LastSampleTime = t
		self.LastInput = x

		self.ErrorSum = self.ITerm/self.Ki if (self.Ki != 0) else 0

		return y
	
	def reset(self, t):
		self.ErrorSum = 0
		self.ITerm = 0
		self.LastSampleTime = t