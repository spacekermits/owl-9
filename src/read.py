import krpc
import numpy as np
import time


class vessel_data(object):
    def __init__(self, vessel):
        self.vessel = vessel

    def g_force(self):
        return self.vessel.flight().g_force

    def altitude(self):
        return self.vessel.flight().surface_altitude

    def long_and_lat(self):
        tup = (self.vessel.flight().latitude, self.vessel.flight().longitude)
        return tup

    def velocity(self):
        return self.vessel.flight().velocity

    def center_of_mass(self):
        return self.vessel.flight().center_of_mass

    def direction(self):
        return self.vessel.flight().direction

    def prograde(self):
        return self.vessel.flight().prograde

    def retrograde(self):
        return self.vessel.flight().retrograde

    def efficiency_of_engines(self):
        return self.vessel.flight().thrust_specific_fuel_consumption

    def maneuver_node(self, time_, prograde_x):
        return self.vessel.control.add_node(time_, prograde=-prograde_x)

    def enable_sas_mode(self, vessel):
        vessel.control.sas = True

    def change_sas_mode(self, vessel, sas_type):
        s = vessel.control.sas_mode
        if sas_type == "maneuver":
            print("Changed sas mode to Maneuver")
            vessel.control.sas_mode = s.maneuver
        if sas_type == "retrograde":
            print("Changed sas mode to Retrograde")
            vessel.control.sas_mode = s.retrograde
        if sas_type == "prograde":
            print("Changed sas mode to Prograde")
            vessel.control.sas_mode = s.prograde
        if sas_type == "Normal":
            print("Changed sas mode to Normal")
            vessel.control.sas_mode = s.normal
        if sas_type == "Anti-Normal":
            print("Changed sas mode to Anti-Normal")
            vessel.control.sas_mode = s.anti_normal

    def de_orbit(self, burn_time):
        self.vessel.control.activate_next_stage()
        self.vessel.control.throttle = 1
        time.sleep(burn_time + 1)
        self.vessel.control.throttle = 0


class Game_data(object):
    def __init__(self, conn):
        self.conn = conn
        self.vessel = 0

    def get_time(self):
        return self.conn.space_center.ut

    def time_warp(self, ut, rate):
        self.conn.space_center.warp_to(ut, rate)

    def get_vessel(self):
        self.vessel = self.conn.space_center.active_vessel
        return self.conn.space_center.active_vessel

    def stream(self, position, refframe):
        return self.conn.add_stream(position, refframe)

    def get_reference_frame(self):
        return self.conn.space_center.active_vessel.orbit.body.reference_frame
