import krpc
import time
import math
import numpy as np
import read
from multiprocessing import Process
from pid import PID
import pprint
import asyncio
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
conn = krpc.connect(rpc_port=50002, stream_port=50003)


vessel = read.Game_data(conn).get_vessel()
game_data = read.Game_data(conn)
vessel_control = read.vessel_data(vessel)
PI = 3.14159
print('Launch!')
# vessel.control.activate_next_stage()
#vessel.auto_pilot.target_pitch_and_heading(90, 90)
# vessel.auto_pilot.engage()
#vessel.control.throttle = 1
time.sleep(1)

# async def control_loop():
#     rn = vessel.orbit.position_at(game_data.get_time(), vessel.orbit.body.reference_frame)
#     print("RN IS ", rn)
#     print("current is ", game_data.get_time())
#     print("current long and lat is", vessel_control.long_and_lat())
#     print("celestial body surface position is", vessel.orbit.body.surface_position(
#         0.18, -11.930, vessel.orbit.body.reference_frame))
#     print("pitch angle is ", vessel.flight().angle_of_attack
#         )
#     #print("vertical velocity is", vessel_control.velocity() * math.sin(vessel.flight().angle_of_attack))
#     #print("velocity is", vessel_control.velocity())
#     print("speed is", vessel.orbit.speed)
#     telem = vessel.flight(vessel.orbit.body.reference_frame)
#     vessel.control.speed_mode = vessel.control.speed_mode.surface

#     time.sleep(0.5)


#     vessel_control.change_sas_mode(vessel, "retrograde")
#     p = PID(0.25, 0.25, 0.025)
#     while vessel.situation is not vessel.situation.landed:
#         descent = telem.surface_altitude / -10.0
#         if descent < -15.0:
#             descent = -15.0
#         p.set_point(descent)
#         vessel.control.throttle = p.compute(telem.vertical_speed)
#         print("TELEM VERT SPEED IS", telem.vertical_speed)
#         print("THRUST THIS MUCH TO COUNTER BALANCE", p.compute(telem.vertical_speed))

#     vessel.control.throttle = 0

def visualize():
    # rn_hist = np.empty((3,))
    # print(rn_hist.shape)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    refframe = vessel.orbit.body.reference_frame
    first_pos = np.array(vessel.position(refframe))
    rn_hist = [0, 0, 0]
    rn = -(np.array(vessel.position(refframe)) - first_pos)
    rn_hist = np.vstack((rn_hist, rn))

    # return
    while(True):

        rn = -(np.array(vessel.position(refframe)) - first_pos)
        rn_hist = np.vstack((rn_hist, rn))
        print(rn_hist)

        ax.plot(rn_hist[:, 0], rn_hist[:, 1], rn_hist[:, 2], color='b')
        plt.draw()
        plt.pause(0.3)


visualize()
