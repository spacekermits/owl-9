import math
import time
import krpc
from math import *

turn_start_alt = 250
turn_end_alt = 45000
target_alt = 150000

conn = krpc.connect(name='Launch into orbit')
vessel = conn.space_center.active_vessel

canvas = conn.ui.stock_canvas
screen_size = canvas.rect_transform.size

panel = canvas.add_panel()
rect = panel.rect_transform
rect.size = (200, 100)
rect.position = (110-(screen_size[0]/2), 0)

text = panel.add_text("Roll: ")
text.rect_transform.position = (0, -20)
text.color = (1, 1, 1)
text.size = 18

roll = conn.add_stream(getattr, vessel.flight(), 'roll')
prev_roll = roll()
text.content = "Roll: {0}".format(roll())


ut = conn.add_stream(getattr, conn.space_center, 'ut') # time (now) in game
alt = conn.add_stream(getattr, vessel.flight(), 'mean_altitude')
ap = conn.add_stream(getattr, vessel.orbit, 'apoapsis_altitude')
stage_2_resources = vessel.resources_in_decouple_stage(stage=7, cumulative=False)
# stage_1_resources = vessel.resources_in_stage
srb_fuel = conn.add_stream(stage_2_resources.amount, 'LiquidFuel')

vessel.control.sas = False
vessel.control.rcs = False
vessel.control.throttle = 1.0

for i in range(1, 3, -1):
    print('{0}...' % i)
    time.sleep(1)
print('Launch!')

# Activate the first stage
vessel.control.activate_next_stage()
vessel.auto_pilot.engage()
vessel.auto_pilot.target_pitch_and_heading(90,90)




srbs_seperated = False
turn_angle = 0
while True:
    cur_roll = roll()

    if abs(cur_roll - prev_roll) > 0.01:
        prev_roll = cur_roll
        text.content = "Roll: {0}".format(cur_roll)
    if(alt() > turn_start_alt and alt() < turn_end_alt):
        frac = (alt() - turn_start_alt) / (turn_end_alt - turn_start_alt)
        new_turn_angle = frac * 90
        if abs(new_turn_angle - turn_angle) > 0.5:
            turn_angle = new_turn_angle
            vessel.auto_pilot.target_pitch_and_heading(90-turn_angle, 90)
            vessel.auto_pilot.target_roll = 0
    # seperate SRBs when they run out of fuel
    if not srbs_seperated:
        if(srb_fuel() < 0.1):
            # stabilize
            vessel_direction = vessel.direction(vessel.surface_reference_frame)
            vessel.auto_pilot.target_direction = vessel_direction
            vessel.auto_pilot.wait() # wait until stable
            
            vessel.control.activate_next_stage() # ditch srbs
            srbs_seperated = True
            print('SRBs seperated')
    # decrease throttle when approaching target ap
    if ap() > target_alt*0.9:
        print('Approaching target apoapsis')
        break
vessel.control.throttle = .25
while ap() < target_alt:
    time.sleep(.1)
print('Target apoapsis reached')
vessel.control.throttle = 0.0

print('Coasting out of athmosphere')
while alt() < 70500: # atmosphere is 70000
    pass

def circularization_dv(vessel) -> float:
    mu = vessel.orbit.body.gravitational_parameter
    r = vessel.orbit.apoapsis
    a1 = vessel.orbit.semi_major_axis
    a2 = r
    v1 = math.sqrt(mu*((2./r)-(1./a1)))
    v2 = math.sqrt(mu*((2./r)-(1./a2)))
    dv = v2 - v1
    return dv


    
# Plan circularization burn (using vis-viva equation)
print('Planning circularization burn')
dv = circularization_dv(vessel)
node = vessel.control.add_node( ut() + vessel.orbit.time_to_apoapsis, prograde=dv )

F = vessel.available_thrust
Isp = vessel.specific_impulse * 9.82
m0 = vessel.mass
m1 = m0 / math.exp(dv/Isp)
flow_rate = F / Isp
burn_time = (m0-m1)/flow_rate

print('Orientating ship for circularization burn')
vessel.auto_pilot.reference_frame = node.reference_frame
vessel.auto_pilot.target_direction = (0,1,0)
vessel.auto_pilot.wait()

print('Waiting until circularization node')
burn_ut = ut() + vessel.orbit.time_to_apoapsis - (burn_time/2.)
lead_time = 5
conn.space_center.warp_to(burn_ut - lead_time)


print('Ready to execute burn')
time_to_ap = conn.add_stream(getattr, vessel.orbit, 'time_to_apoapsis')
while time_to_ap() - (burn_time/2.) > 0:
    pass

print('Executing burn')
vessel.control.throttle = 1.
time.sleep(burn_time - 0.1)
print('Fine tuning')
vessel.control.throttle = .05
remaining_burn = conn.add_stream(node.remaining_burn_vector, node_reference_frame)
while remaining_burn()[1] > 0:
    pass
vessel.control.throttle = 0.
node.remove()

print('Launch complete!')