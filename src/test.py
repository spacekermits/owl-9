import krpc
import time
import read_sensor
conn = krpc.connect()
vessel = conn.space_center.active_vessel

print('Launch!')
vessel.control.activate_next_stage()

vessel.auto_pilot.target_pitch_and_heading(90, 90)
vessel.auto_pilot.engage()
# vessel.control.throttle = 1
time.sleep(1)

a = read_sensor.data(vessel)

while True:
    refframe = vessel.orbit.body.reference_frame
    ref = vessel.orbit.body
    position = conn.add_stream(vessel.position, refframe)
    # print(ref)
    # print("Time to Apoapsis", vessel.orbit.time_to_apoapsis)
    # print(vessel.flight().retrograde)
    print(a)
