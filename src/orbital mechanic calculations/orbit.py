#%%
import numpy as np
from math import *

G = 6.678*10**(-11)
M_earth = 5.972*10**24 # kg
r_earth = 6378*10**3 # m
r1 = 300 * 10**3 + r_earth # start at 300km above surface
r2 = 4.2164 * 10**7 # geostationary orbit
μ = M_earth*G


#%%
# transfer to geostationary orbit
def get_dV(r1, r2):
  V_first = sqrt(μ*1/r1)
  print('V_first:', V_first, 'm/s')

  rs = (r1 + r2)/2
  Vp = sqrt(μ*(2/r1 - 1/rs))
  print('Vp: ', Vp, 'm/s')

  Va = sqrt(μ*(2/r2 - 1/rs))
  print('Va: ', Va, 'm/s')

  dV_1 = Vp - V_first
  
  Vq = sqrt(μ*1/r2)
  print('Vq: ', Vq)
  dV_2 = Vq - Va
  dV = dV_1 + dV_2

  return dV


dV = get_dV(r1, r2)
print('dV for geostationary transfer: ', dV, 'm/s')


