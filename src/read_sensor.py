import krpc
import numpy as np


class data(object):
    def __init__(self, vessel):
        self.vessel = vessel

    def g_force(self):
        return self.vessel.flight().g_force

    def altitude(self):
        return self.vessel.flight().surface_altitude

    def long_and_lat(self):
        tup = (self.vessel.flight().latitude, self.vessel.flight().longitude)
        return tup

    def velocity(self):
        return self.vessel.flight().velocity

    def center_of_mass(self):
        return self.vessel.flight().center_of_mass

    def direction(self):
        return self.vessel.flight().direction

    def prograde(self):
        return self.vessel.flight().prograde

    def retrograde(self):
        return self.vessel.flight().retrograde

    def efficiency_of_engines(self):
        return self.vessel.flight().thrust_specific_fuel_consumption
