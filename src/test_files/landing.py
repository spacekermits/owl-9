#%%
import krpc
import numpy as np
from math import *
import time

import threading
import sys
from typing import NamedTuple

conn: krpc.client.Client = None
VAB_lat = -(0+(5.0/60)+(48.38/60/60))
VAB_lon = -(74+(37.0/60)+(12.2/60/60))
VAB_alt = 111

class Site(NamedTuple):
    lat: float
    lon: float
    alt: float

def deorbit(vessel, eccentricity):
    '''executes a deorbit burn until the given eccentricity
    is achieved.   If periapsis remains above the equatorial
    radius, it continues burning.
    '''
    pe_threshold = 0
    print("deorbiting the vessel...")
    vessel.control.speed_mode = vessel.control.speed_mode.surface
    auto_pilot = vessel.auto_pilot
    auto_pilot.sas = True
    time.sleep(.1)
    auto_pilot.sas_mode = auto_pilot.sas_mode.retrograde

    while (vessel.orbit.eccentricity < eccentricity or
           vessel.orbit.periapsis_altitude > pe_threshold):
        vessel.control.throttle = 1.0
        # print("eccentricity:", vessel.orbit.eccentricity)
        # print("pe:", vessel.orbit.periapsis_altitude)
        time.sleep(.01)
    vessel.control.throttle = 0.0

def landing_ref(site: Site, vessel): # returns reference frame
    create_relative = conn.space_center.ReferenceFrame.create_relative
    body = vessel.orbit.body
    landing_pos = body.surface_position(site.lat, site.lon, body.reference_frame)
    q_long = (
    0,
    sin(-site.lon * 0.5 * pi / 180),
    0,
    cos(-site.lon * 0.5 * pi / 180)
    )
    q_lat = (
        0,
        0,
        sin(site.lat * 0.5 * pi / 180),
        cos(site.lat * 0.5 * pi / 180)
    )
    landing_reference_frame = \
        create_relative(
            create_relative(
                create_relative(
                    body.reference_frame,
                    landing_pos,
                    q_long
                ),
                (0,0,0), q_lat
            ),
            (site.alt, 0, 0)
        )
    conn.drawing.add_line((0,0,0), (1,0,0), landing_reference_frame)
    conn.drawing.add_line((0,0,0), (0,1,0), landing_reference_frame)
    conn.drawing.add_line((0,0,0), (0,0,1), landing_reference_frame)


def main(argv):
    global conn
    conn = krpc.connect(name='Landing')
    vessel = conn.space_center.active_vessel
    site = Site(lat=0.0,lon=0.0,alt=0.0)
    landing_ref(site, vessel)
    # deorbit(vessel, 0)
    body = vessel.orbit.body
    # landing_pos = body.






if __name__ == "__main__":
    main(sys.argv)

#%%
