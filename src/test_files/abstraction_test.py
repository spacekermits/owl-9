#%%
import os, sys
sys.path.append("../")

import krpc
import numpy as np

import src.fuel as fuel

conn = krpc.connect(name='autopilot')

vessel = conn.space_center.active_vessel


print(vessel.resources.names)

print(fuel.resource_types(vessel))
print(fuel.get_fuel(vessel))


#%%
