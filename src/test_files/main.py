#%%
import krpc
import numpy as np

conn = krpc.connect(name='Hello world')
print(conn.krpc.get_status().version)


#%%
conn = krpc.connect(
    name='My Example Program',
    # address='192.168.1.10',
    address='127.0.0.1',
    rpc_port=50000, stream_port=50001)
print(conn.krpc.get_status().version)

#%%
vessel = conn.space_center.active_vessel
abort = conn.add_stream(getattr, vessel.control, 'abort')


def check_abort1(x):
    vessel.auto_pilot.target_pitch_and_heading(90, 90)
    vessel.auto_pilot.engage()
    while True:
        vessel.control.throttle = 1
        


def check_abort2(x):
    print('Abort 2 called with a value of', x)

abort.add_callback(check_abort1)
abort.add_callback(check_abort2)
# abort.start()

#%%

vessel = conn.space_center.active_vessel
print("active vessel name:", vessel.name)
flight_info = vessel.flight()

refframe = vessel.orbit.body.reference_frame
position = conn.add_stream(vessel.position, refframe)
velocity = conn.add_stream(vessel.velocity, refframe)
while True:
    print("{")
    print("\tposition relative to ", vessel.orbit.body.name, " = ", position())
    vel = np.array(velocity())
    euler_vel = np.linalg.norm(vel)
    print("\tspeed relative to ", vessel.orbit.body.name, " = ", euler_vel)
    print("}")


#%%





#%%

