import krpc
from flight_plan import FlightPlan
from flight_plan import Maneuver

conn = krpc.connect()


def hello_world():
    print("hello world")


def test():
    print("YO IM TESTING")


flight = FlightPlan()
flight.add_stage(Maneuver(name="hello world", procedure=hello_world))
flight.add_stage(Maneuver(name="test", procedure=test))

# flight.add_stage(test)
flight.execute_all()


class test_class(object):
    def __init__(self):
        pass

    def hello(self):
        print("hello BOIS")
