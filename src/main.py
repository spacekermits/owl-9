import krpc
import time
import json
import sys
sys.path.append('orbital mechanic calculations')

import read as rd
import urllib.request

from flight_plan import Maneuver, FlightPlan
from flight_plan import JSON_object
from maneuvers import bruh

PI = 3.14159

def main():

    # user_input = input("Enter North Hemisphere, Equator or South Hemisphere: ")
    # user_input = user_input.lower()

    # example on how to add maneuvers
    # maneuver = Maneuver('Landing', procedure=landing)
    # maneuver2 = Maneuver('de-orbit', procedure=de_orbit)
    # maneuver3 = Maneuver(name='inclination', procedure=inclination,
    #                      user_input=user_input, angle=45)

    # make_json()
    txt = urllib.request.urlopen(
        'https://raw.githubusercontent.com/MuhsinFatih/ksp_maneuvers/master/maneuvers').read()
    txt = txt.decode("utf-8")

    '''MANEUVERS CALLED HERE'''
    # First you need to create a JSON file to store your maneuver function calls
    ### IT IS VERY IMPORTANT THAT YOU CREATE THIS DICTIONARY AND MAKE IT TO JSON ###
    plan1 = [{'name': 'circularize', 'at': 'apoapsis'},{'name': 'transfer_to_geostationary'}]
    plan2 = [{'name': 'escape'}]
    plan3 = [{'name': 'launch_into_orbit'},
                  {'name': 'circularize', 'at': 'apoapsis'},
                  {'name': 'wait', 'seconds': 60},
                  {'name': 'transfer_to_geostationary'},
                  {'name': 'wait', 'seconds': 60},
                  {'name': 'escape'}]
    plan4 = [{'name': 'circularize', 'at': 'apoapsis'},
                  {'name': 'transfer_to_geostationary'},
                  {'name': 'escape'}]
    
    conn = krpc.connect(rpc_port=50000, stream_port=50001)
    vessel = conn.space_center.active_vessel
    # make the class object where your maneuver lives
    b = bruh(conn, vessel)
    # make the json from the given dictionary
    make_json(plan3)
    # put your 'object' into JSON_object which will call your 'methods' in the class, or 'maneuvers'
    tests = JSON_object(b)
    # this will give you the function names from the JSON file you just created as well as the function parameters
    fn_names, params = tests.read_json(filename='data.json')
    # create the maneuvers given the function names and their function parameters. THEN it executes all the maneuvers THEN YOU DONE!
    tests.create_maneuvers(fn_names=fn_names, parameters=params)









    # f = FlightPlan()
    # f.add_stage(maneuver3)
    # f.add_stage(maneuver2)
    # f.add_stage(maneuver)
    # f.execute_all()
    # list_available_maneuvers()
    # inclination("north", 80)
    # de_orbit()


def make_json(list_of_user_config_dict=None):

    if list_of_user_config_dict is None:
        data = {}
        data['maneuvers'] = []
        data['maneuvers'].append({'name': 'inclination', 'angle': 45, 'user_input': 'north'})
        data['maneuvers'].append({'name': 'de_orbit'})
        data['maneuvers'].append({'name': 'landing'})
        data['maneuvers'].append({'name': 'list_available_maneuvers'})
        with open('data.json', 'w') as outfile:
            json.dump(data, outfile)
    else:
        data = {}
        data['maneuvers'] = []
        for i in list_of_user_config_dict:
            data['maneuvers'].append(i)
        with open('data.json', 'w') as outfile:
            json.dump(data, outfile)


if __name__ == '__main__':
    main()
