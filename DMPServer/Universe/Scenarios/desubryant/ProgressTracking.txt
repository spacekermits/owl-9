name = ProgressTracking
scene = 7, 8, 5
Progress
{
	FirstLaunch
	{
		completed = 5618.3546010872851
	}
	FirstCrewToSurvive
	{
		completed = 5618.3546010872851
		crew
		{
			crews = 
		}
	}
	TowerBuzz
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	RecordsAltitude
	{
		completed = 1227.7270379545932
		record = 70000
	}
	RecordsDepth
	{
		completed = 5618.3546010872851
		record = 0
	}
	RecordsSpeed
	{
		completed = 5151.3870703198581
		record = 2500
	}
	RecordsDistance
	{
		completed = 1227.7270379545932
		record = 100000
	}
	ReachedSpace
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	KSCLanding
	{
		completed = 5618.3546010872851
	}
	RunwayLanding
	{
		completed = 5618.3546010872851
	}
	LaunchPadLanding
	{
		completed = 5618.3546010872851
	}
	POIKerbinKSC2
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinIslandAirfield
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinUFO
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinPyramids
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinMonolith00
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinMonolith01
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinMonolith02
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinDesert_Airfield
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinWoomerang_Launch_Site
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunArmstrongMemorial
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunUFO
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunRockArch00
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunRockArch01
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunRockArch02
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunMonolith00
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunMonolith01
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunMonolith02
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIDunaPyramid
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIDunaMSL
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIDunaFace
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMinmusMonolith00
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POITyloCave
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIVallIcehenge
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIBopDeadKraken
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMohoRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIEveRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIGillyRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIKerbinRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMunRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIMinmusRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIDunaRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIIkeRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIDresRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POILaytheRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIVallRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POITyloRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIBopRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIPolRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	POIEelooRandolith
	{
		completed = 5618.3546010872851
		vessel
		{
			name = 
			flag = 
		}
	}
	Sun
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Kerbin
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 1257.687037954566
			vessel
			{
				name = #autoLOC_501218
				flag = Squad/Flags/default
			}
			crew
			{
				crews = Chadcal Kerman, Addard Kerman, Adnie Kerman, Bill Kerman
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Mun
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5614.3546010871978
			vessel
			{
				name = #autoLOC_501252
				flag = Squad/Flags/default
			}
			crew
			{
				crews = Doley Kerman, Bob Kerman
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Minmus
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Moho
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Eve
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Duna
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Ike
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Jool
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Laythe
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Vall
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Bop
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Tylo
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Gilly
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Pol
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Dres
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
	Eeloo
	{
		completed = 5618.3546010872851
		Orbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Escape
		{
			completed = 5618.3546010872851
		}
		Landing
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Splashdown
		{
			completed = 5618.3546010872851
		}
		Science
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Rendezvous
		{
			completed = 5618.3546010872851
		}
		Docking
		{
			completed = 5618.3546010872851
		}
		Spacewalk
		{
			completed = 5618.3546010872851
		}
		SurfaceEVA
		{
			completed = 5618.3546010872851
		}
		FlagPlant
		{
			completed = 5618.3546010872851
		}
		BaseConstruction
		{
			completed = 5618.3546010872851
			base
			{
				name = 
				flag = 
			}
		}
		StationConstruction
		{
			completed = 5618.3546010872851
			station
			{
				name = 
				flag = 
			}
		}
		CelestialBodyTransfer
		{
			completed = 5618.3546010872851
		}
		ReturnFromOrbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flight
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Suborbit
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		Flyby
		{
			completed = 5618.3546010872851
		}
		ReturnFromFlyBy
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
		ReturnFromSurface
		{
			completed = 5618.3546010872851
			vessel
			{
				name = 
				flag = 
			}
		}
	}
}
