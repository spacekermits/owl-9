pid = ce6b413fed97487a97b06f5b9d4257e4
persistentId = 212790219
name = Ast. IIW-330
type = SpaceObject
sit = ORBITING
landed = False
skipGroundPositioning = False
vesselSpawning = True
launchedFrom = 
landedAt = 
displaylandedAt = 
splashed = False
met = 5488.1694184391981
lct = 5488.1694184391981
lastUT = -1
distanceTraveled = 0
root = 0
lat = 0.047055890480398307
lon = -176.8937334807764
alt = 15244645768.68685
hgt = -1
nrm = 0,1,0
rot = -0.581633031,0.407574177,-0.556544483,0.431096941
CoM = 0,0,0
stg = 0
prst = False
ref = 0
ctrl = False
PQSMin = 0
PQSMax = 0
ORBIT
{
	SMA = 14687154498.79603
	ECC = 0.082374004083642455
	INC = 0.14535615263283597
	LPE = 154.88401462246082
	LAN = 168.79123550434667
	MNA = 0.41057371871525
	EPH = 4385648.0039490908
	REF = 0
}
PART
{
	name = PotatoRoid
	cid = 0
	uid = 0
	mid = 0
	persistentId = 2537387028
	launchID = 0
	parent = 0
	position = 0,0,0
	rotation = 0,0,0,1
	mirror = 1,1,1
	symMethod = Radial
	istg = 0
	resPri = 0
	dstg = 0
	sqor = 0
	sepI = 0
	sidx = 0
	attm = 0
	srfN = None, -1
	mass = 150
	shielded = False
	temp = -1
	tempExt = 0
	tempExtUnexp = 0
	staticPressureAtm = 0
	expt = 1
	state = 0
	attached = True
	autostrutMode = Off
	rigidAttachment = False
	flag = 
	rTrf = 
	modCost = 0
}
ACTIONGROUPS
{
}
DISCOVERY
{
	state = 1
	lastObservedTime = 5488.1694184391981
	lifetime = Infinity
	refTime = Infinity
	size = 4
}
FLIGHTPLAN
{
}
CTRLSTATE
{
}
VESSELMODULES
{
	FlightIntegrator
	{
	}
	CommNetVessel
	{
		controlState = None
		canComm = True
	}
}
