pid = f1de41e355c44dda99e05db757ef555a
persistentId = 3499597146
name = Ast. CWC-027
type = SpaceObject
sit = ORBITING
landed = False
skipGroundPositioning = False
vesselSpawning = True
launchedFrom = 
landedAt = 
displaylandedAt = 
splashed = False
met = 5315.0327868665108
lct = 5315.0327868665108
lastUT = -1
distanceTraveled = 0
root = 0
lat = -0.42507654443185622
lon = -76.800360417982915
alt = 16951180126.635792
hgt = -1
nrm = 0,1,0
rot = 0.431754827,-0.431280702,-0.562379003,0.557955623
CoM = 0,0,0
stg = 0
prst = False
ref = 0
ctrl = False
PQSMin = 0
PQSMax = 0
ORBIT
{
	SMA = 15786973227.011175
	ECC = 0.1432116606536695
	INC = 0.94702175619961526
	LPE = 341.88617760319926
	LAN = 80.960606724771424
	MNA = 0.21172604633451778
	EPH = 15828105.070349745
	REF = 0
}
PART
{
	name = PotatoRoid
	cid = 0
	uid = 0
	mid = 0
	persistentId = 2910850951
	launchID = 0
	parent = 0
	position = 0,0,0
	rotation = 0,0,0,1
	mirror = 1,1,1
	symMethod = Radial
	istg = 0
	resPri = 0
	dstg = 0
	sqor = 0
	sepI = 0
	sidx = 0
	attm = 0
	srfN = None, -1
	mass = 150
	shielded = False
	temp = -1
	tempExt = 0
	tempExtUnexp = 0
	staticPressureAtm = 0
	expt = 1
	state = 0
	attached = True
	autostrutMode = Off
	rigidAttachment = False
	flag = 
	rTrf = 
	modCost = 0
}
ACTIONGROUPS
{
}
DISCOVERY
{
	state = 1
	lastObservedTime = 5315.0327868665108
	lifetime = Infinity
	refTime = Infinity
	size = 3
}
FLIGHTPLAN
{
}
CTRLSTATE
{
}
VESSELMODULES
{
	FlightIntegrator
	{
	}
	CommNetVessel
	{
		controlState = None
		canComm = True
	}
}
