pid = bd3caa60f1c64c19ac7f9bd1028e9dfc
persistentId = 4175179403
name = Ast. HDZ-009
type = SpaceObject
sit = ORBITING
landed = False
skipGroundPositioning = False
vesselSpawning = True
launchedFrom = 
landedAt = 
displaylandedAt = 
splashed = False
met = 1313.8746216740562
lct = 1313.8746216740562
lastUT = -1
distanceTraveled = 0
root = 0
lat = 0.14552476213864796
lon = -149.71198143955505
alt = 14011386940.26206
hgt = -1
nrm = 0,1,0
rot = 0.177674994,0.789809644,0.553889334,0.194522098
CoM = 0,0,0
stg = 0
prst = False
ref = 0
ctrl = False
PQSMin = 0
PQSMax = 0
ORBIT
{
	SMA = 13913666321.713037
	ECC = 0.025904341796891418
	INC = 0.44542334488403917
	LPE = 345.30108831215597
	LAN = 50.451832919885078
	MNA = 0.37760668670326314
	EPH = 24505267.928718738
	REF = 0
}
PART
{
	name = PotatoRoid
	cid = 0
	uid = 0
	mid = 0
	persistentId = 1565930175
	launchID = 0
	parent = 0
	position = 0,0,0
	rotation = 0,0,0,1
	mirror = 1,1,1
	symMethod = Radial
	istg = 0
	resPri = 0
	dstg = 0
	sqor = 0
	sepI = 0
	sidx = 0
	attm = 0
	srfN = None, -1
	mass = 150
	shielded = False
	temp = -1
	tempExt = 0
	tempExtUnexp = 0
	staticPressureAtm = 0
	expt = 1
	state = 0
	attached = True
	autostrutMode = Off
	rigidAttachment = False
	flag = 
	rTrf = 
	modCost = 0
}
ACTIONGROUPS
{
}
DISCOVERY
{
	state = 1
	lastObservedTime = 1313.8746216740562
	lifetime = Infinity
	refTime = Infinity
	size = 4
}
FLIGHTPLAN
{
}
CTRLSTATE
{
}
VESSELMODULES
{
	FlightIntegrator
	{
	}
	CommNetVessel
	{
		controlState = None
		canComm = True
	}
}
