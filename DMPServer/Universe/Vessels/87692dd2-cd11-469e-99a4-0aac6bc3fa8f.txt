pid = 87692dd2cd11469e99a40aac6bc3fa8f
persistentId = 361504101
name = Ast. GUS-968
type = SpaceObject
sit = ORBITING
landed = False
skipGroundPositioning = False
vesselSpawning = True
launchedFrom = 
landedAt = 
displaylandedAt = 
splashed = False
met = 5217.7857569326461
lct = 5217.7857569326461
lastUT = -1
distanceTraveled = 0
root = 0
lat = 0.94367012564568697
lon = -14.656661506652101
alt = 14018789594.041182
hgt = -1
nrm = 0,1,0
rot = 0.589389563,0.527131319,0.0998917744,0.603965342
CoM = 0,0,0
stg = 0
prst = False
ref = 0
ctrl = False
PQSMin = 0
PQSMax = 0
ORBIT
{
	SMA = 15632292634.042021
	ECC = 0.13253468040260663
	INC = 1.019888334495368
	LPE = 12.405082879580265
	LAN = 281.98480272763709
	MNA = -0.19234891670962379
	EPH = 20971013.32024968
	REF = 0
}
PART
{
	name = PotatoRoid
	cid = 0
	uid = 0
	mid = 0
	persistentId = 2689913485
	launchID = 0
	parent = 0
	position = 0,0,0
	rotation = 0,0,0,1
	mirror = 1,1,1
	symMethod = Radial
	istg = 0
	resPri = 0
	dstg = 0
	sqor = 0
	sepI = 0
	sidx = 0
	attm = 0
	srfN = None, -1
	mass = 150
	shielded = False
	temp = -1
	tempExt = 0
	tempExtUnexp = 0
	staticPressureAtm = 0
	expt = 1
	state = 0
	attached = True
	autostrutMode = Off
	rigidAttachment = False
	flag = 
	rTrf = 
	modCost = 0
}
ACTIONGROUPS
{
}
DISCOVERY
{
	state = 1
	lastObservedTime = 5217.7857569326461
	lifetime = Infinity
	refTime = Infinity
	size = 0
}
FLIGHTPLAN
{
}
CTRLSTATE
{
}
VESSELMODULES
{
	FlightIntegrator
	{
	}
	CommNetVessel
	{
		controlState = None
		canComm = True
	}
}
