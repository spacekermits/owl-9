pid = e246c565dc6441bc960578ce2bcdbfe1
persistentId = 2422478615
name = Ast. SEM-145
type = SpaceObject
sit = ORBITING
landed = False
skipGroundPositioning = False
vesselSpawning = True
launchedFrom = 
landedAt = 
displaylandedAt = 
splashed = False
met = 903.54863927613053
lct = 903.54863927613053
lastUT = -1
distanceTraveled = 0
root = 0
lat = -0.67603113234417578
lon = -134.62782321602538
alt = 13522064336.608398
hgt = -1
nrm = 0,1,0
rot = -0.55853039,-0.726204753,0.248123288,0.314809918
CoM = 0,0,0
stg = 0
prst = False
ref = 0
ctrl = False
PQSMin = 0
PQSMax = 0
ORBIT
{
	SMA = 13988071048.382397
	ECC = 0.03541326491920483
	INC = 1.2896486508291607
	LPE = 35.875051840009334
	LAN = 257.73507713932611
	MNA = -0.54036443070996931
	EPH = 29677475.042865325
	REF = 0
}
PART
{
	name = PotatoRoid
	cid = 0
	uid = 0
	mid = 0
	persistentId = 1184434773
	launchID = 0
	parent = 0
	position = 0,0,0
	rotation = 0,0,0,1
	mirror = 1,1,1
	symMethod = Radial
	istg = 0
	resPri = 0
	dstg = 0
	sqor = 0
	sepI = 0
	sidx = 0
	attm = 0
	srfN = None, -1
	mass = 150
	shielded = False
	temp = -1
	tempExt = 0
	tempExtUnexp = 0
	staticPressureAtm = 0
	expt = 1
	state = 0
	attached = True
	autostrutMode = Off
	rigidAttachment = False
	flag = 
	rTrf = 
	modCost = 0
}
ACTIONGROUPS
{
}
DISCOVERY
{
	state = 1
	lastObservedTime = 903.54863927613053
	lifetime = Infinity
	refTime = Infinity
	size = 4
}
FLIGHTPLAN
{
}
CTRLSTATE
{
}
VESSELMODULES
{
	FlightIntegrator
	{
	}
	CommNetVessel
	{
		controlState = None
		canComm = True
	}
}
