pid = ff1462493e9842d896b8398a388d1ea0
persistentId = 103433411
name = Ast. FNO-185
type = SpaceObject
sit = ORBITING
landed = False
skipGroundPositioning = False
vesselSpawning = True
launchedFrom = 
landedAt = 
displaylandedAt = 
splashed = False
met = 1003.0086392760401
lct = 1003.0086392760401
lastUT = -1
distanceTraveled = 0
root = 0
lat = 1.274699301825498
lon = -102.25193862724082
alt = 13729444301.119921
hgt = -1
nrm = 0,1,0
rot = 0.63015157,-0.733048141,-0.206334889,0.151575893
CoM = 0,0,0
stg = 0
prst = False
ref = 0
ctrl = False
PQSMin = 0
PQSMax = 0
ORBIT
{
	SMA = 14564128849.470026
	ECC = 0.066315911837449518
	INC = 1.5208987056130605
	LPE = 0.2185077943877512
	LAN = 201.64767574848281
	MNA = -0.0084329167938893487
	EPH = 18954350.348585188
	REF = 0
}
PART
{
	name = PotatoRoid
	cid = 0
	uid = 0
	mid = 0
	persistentId = 3869577590
	launchID = 0
	parent = 0
	position = 0,0,0
	rotation = 0,0,0,1
	mirror = 1,1,1
	symMethod = Radial
	istg = 0
	resPri = 0
	dstg = 0
	sqor = 0
	sepI = 0
	sidx = 0
	attm = 0
	srfN = None, -1
	mass = 150
	shielded = False
	temp = -1
	tempExt = 0
	tempExtUnexp = 0
	staticPressureAtm = 0
	expt = 1
	state = 0
	attached = True
	autostrutMode = Off
	rigidAttachment = False
	flag = 
	rTrf = 
	modCost = 0
}
ACTIONGROUPS
{
}
DISCOVERY
{
	state = 1
	lastObservedTime = 1003.0086392760401
	lifetime = Infinity
	refTime = Infinity
	size = 4
}
FLIGHTPLAN
{
}
CTRLSTATE
{
}
VESSELMODULES
{
	FlightIntegrator
	{
	}
	CommNetVessel
	{
		controlState = None
		canComm = True
	}
}
