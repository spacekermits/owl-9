name = Chadcal Kerman
gender = Male
type = Crew
trait = Pilot
brave = 0.654789686
dumb = 0.358887672
badS = False
veteran = False
tour = False
state = Assigned
inactive = False
inactiveTimeEnd = 0
gExperienced = 0
outDueToG = False
ToD = 0
idx = 0
extraXP = 0
hasHelmetOn = False
hasNeckRingOn = False
completedFirstEVA = False
hero = False
CAREER_LOG
{
	flight = 0
}
FLIGHT_LOG
{
	flight = 0
	0 = Flight,Kerbin
	0 = Suborbit,Kerbin
	0 = Orbit,Kerbin
}
