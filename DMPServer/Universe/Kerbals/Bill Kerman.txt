name = Bill Kerman
gender = Male
type = Crew
trait = Engineer
brave = 0.5
dumb = 0.800000012
badS = False
veteran = True
tour = False
state = Assigned
inactive = False
inactiveTimeEnd = 0
gExperienced = 0
outDueToG = False
ToD = 0
idx = 3
extraXP = 0
hasHelmetOn = False
hasNeckRingOn = False
completedFirstEVA = False
hero = True
CAREER_LOG
{
	flight = 0
}
FLIGHT_LOG
{
	flight = 0
	0 = Flight,Kerbin
	0 = Suborbit,Kerbin
	0 = Orbit,Kerbin
}
